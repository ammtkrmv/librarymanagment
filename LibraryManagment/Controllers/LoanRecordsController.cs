﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagment.Data;
using LibraryManagment.Models;

namespace LibraryManagment.Controllers
{
    public class LoanRecordsController : Controller
    {
        private readonly LibraryManagmentContext _context;

        public LoanRecordsController(LibraryManagmentContext context)
        {
            _context = context;
        }

        // GET: LoanRecords
        public async Task<IActionResult> Index()
        {
            var libraryManagmentContext = _context.LoanRecord.Include(l => l.Book).Include(l => l.Reader);
            return View(await libraryManagmentContext.ToListAsync());
        }

        // GET: LoanRecords/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loanRecord = await _context.LoanRecord
                .Include(l => l.Book)
                .Include(l => l.Reader)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (loanRecord == null)
            {
                return NotFound();
            }

            return View(loanRecord);
        }

        // GET: LoanRecords/Create
        public IActionResult Create()
        {
            ViewData["BookId"] = new SelectList(_context.Book, "Id", "Author");
            ViewData["ReaderId"] = new SelectList(_context.Reader, "Id", "Email");
            return View();
        }

        // POST: LoanRecords/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BookId,ReaderId,IssueDate,ReturnDate")] LoanRecord loanRecord)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loanRecord);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Book, "Id", "Author", loanRecord.BookId);
            ViewData["ReaderId"] = new SelectList(_context.Reader, "Id", "Email", loanRecord.ReaderId);
            return View(loanRecord);
        }

        // GET: LoanRecords/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loanRecord = await _context.LoanRecord.FindAsync(id);
            if (loanRecord == null)
            {
                return NotFound();
            }
            ViewData["BookId"] = new SelectList(_context.Book, "Id", "Author", loanRecord.BookId);
            ViewData["ReaderId"] = new SelectList(_context.Reader, "Id", "Email", loanRecord.ReaderId);
            return View(loanRecord);
        }

        // POST: LoanRecords/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,BookId,ReaderId,IssueDate,ReturnDate")] LoanRecord loanRecord)
        {
            if (id != loanRecord.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loanRecord);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoanRecordExists(loanRecord.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Book, "Id", "Author", loanRecord.BookId);
            ViewData["ReaderId"] = new SelectList(_context.Reader, "Id", "Email", loanRecord.ReaderId);
            return View(loanRecord);
        }

        // GET: LoanRecords/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loanRecord = await _context.LoanRecord
                .Include(l => l.Book)
                .Include(l => l.Reader)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (loanRecord == null)
            {
                return NotFound();
            }

            return View(loanRecord);
        }

        // POST: LoanRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loanRecord = await _context.LoanRecord.FindAsync(id);
            if (loanRecord != null)
            {
                _context.LoanRecord.Remove(loanRecord);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LoanRecordExists(int id)
        {
            return _context.LoanRecord.Any(e => e.Id == id);
        }
    }
}
