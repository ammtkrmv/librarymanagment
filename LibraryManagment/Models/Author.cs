﻿using System.ComponentModel.DataAnnotations;

namespace LibraryManagment.Models
{
    public class Author
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
