﻿using System.ComponentModel.DataAnnotations;

namespace LibraryManagment.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Title { get; set; }
        [Required]
        [StringLength(255)]
        public  string Author { get; set; }
    }
}
