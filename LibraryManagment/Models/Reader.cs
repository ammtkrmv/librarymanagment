﻿using System.ComponentModel.DataAnnotations;

namespace LibraryManagment.Models
{
    public class Reader
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string LibraryCardNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
